(function() {
  'use strict';

  angular
    .module('chat')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($state, userAPI) {
    var vm = this;
    vm.chat = chat;

    activate();

    function activate() {
      userAPI.recents()
        .then(function (users) {
          vm.users = users;
        });
    }

    function chat(user){
      $state.go('app.chat', {user: user.mention});
    }
  }
})();
