(function() {
  'use strict';

  angular
    .module('chat', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ui.router',
      'ngMaterial',
      'toastr',
      'ui.gravatar'
    ]);

})();
