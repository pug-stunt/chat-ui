(function() {
  'use strict';

  angular
    .module('chat')
    .service('userAPI', userAPI);

  function userAPI($q) {
    var service = {
      recents: recents
    };

    return service;

    function recents() {
      return $q.resolve([
        {name: "Fernando Falci", mention: "falci", email: "falci@falci.me"},
        {name: "Paulo Almeida", mention: "fisca", email: "almeida.paulorocha@gmail.com"},
        {name: "Evandro Oliveira", mention: "presidente", email: "evandrobigfriend@gmail.com"}
      ]);
    }
  }

})();
