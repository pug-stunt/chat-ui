(function() {
  'use strict';

  angular
    .module('chat')
    .service('chatAPI', chatAPI);

  function chatAPI($q) {
    var service = {
      history: history
    };

    return service;

    function history(mention) {
      var fisca = {
          name: 'Paulo Almeida',
          email: 'almeida.paulorocha@gmail.com'
        },
        falci = {
            name: 'Fernando Falci',
            email: 'falci@falci.me'
        },
        sender = (mention === 'falci' ? falci : fisca),
        chat = {
          name: sender.name,
          messages: [
            {
              date: new Date(2016, 6, 13, 16, 45, 10),
              text: 'Olar',
              sender: sender
            },
            {
              date: new Date(2016, 6, 13, 16, 45, 12),
              text: 'Test',
              sender: sender
            }
          ]
        };

      return $q.resolve(chat);
    }
  }

})();
