(function() {
  'use strict';

  angular
    .module('chat')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        abstract: true,
        templateUrl: 'app/components/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('app.home', {
        url: '/home',
        templateUrl: 'app/components/home/home.html',
        controller: 'HomeController',
        controllerAs: '$ctrl'
      })
      .state('app.chat', {
        url: '/chat/:user',
        templateUrl: 'app/components/chat/chat.html',
        controller: 'ChatController',
        controllerAs: '$ctrl'
      });

    $urlRouterProvider.otherwise('/home');
  }

})();
