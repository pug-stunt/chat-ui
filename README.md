# Chat UI

Just another web chat :)


## Features
* [x] Google's Material Design
* [ ] OAuth

## Requirements
* Node
* Bower (`npm i -g bower`)
* Gulp (`npm i -g gulp`)

## Installing
Note: there is no _dist_ version yet; Only development, and we are using the _master_ branch;

```
git clone git@gitlab.com:pug-stunt/chat-ui.git
cd chat-ui
npm install
bower install
gulp serve
```


**PRs are welcome**
